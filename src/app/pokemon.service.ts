import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Pokemon } from 'src/types/pokemon';

@Injectable({
  providedIn: 'root'
})
export class PokemonService {
  private defaultLimit = 20;
  constructor(private http: HttpClient) { }

  public getPokemons(): Observable<Array<Pokemon>> {
    return this.http.get<Array<Pokemon>>(`${environment.basePokemonUrl}pokemon?limit=${this.defaultLimit}`)
  }

  public getSavedPokemons(): Observable<Array<Pokemon>> {
    return this.http.get<Array<any>>(`${environment.baseBackEndUrl}pokemon`)
  }

  public addPokemon(pokemon: Pokemon): Observable<Pokemon> {
    return this.http.post<Pokemon>(`${environment.baseBackEndUrl}pokemon/add`, pokemon)
  }

  public deletePokemon(pokemonId: number): Observable<Pokemon> {
    return this.http.delete<Pokemon>(`${environment.baseBackEndUrl}pokemon/delete/${pokemonId}`)
  }
}
