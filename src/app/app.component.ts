import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Pokemon } from 'src/types/pokemon';
import { PokemonService } from './pokemon.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'pokedexApp';
  displayedColumns: Array<string> = ['name', 'save'];
  displayedColumnsForSavedPokemon: Array<string> = ['name', 'delete'];
  public showSavedPokemons = false;
  public pokemons: Array<Pokemon> = [];
  public savedPokemons: Array<Pokemon> = [];
  public isLoading = false;

  constructor(private pokemonService: PokemonService) { }

  ngOnInit(): void {
    this.getSavedPokemons();
    this.getPokemons();
  }

  add(pokemon: Pokemon) {
    const isAlreadyAdded = this.savedPokemons.find(savedPokemon => savedPokemon.url == pokemon.url)

    if (!isAlreadyAdded) {
      this.pokemonService.addPokemon(pokemon).subscribe(
        (response: any) => {
          this.getSavedPokemons();
        },
        (error: HttpErrorResponse) => {
          console.error(error.message);
        }
      )
    }
  }

  delete(pokemon: Pokemon) {
    if (pokemon.id) {
      this.pokemonService.deletePokemon(pokemon.id).subscribe(
        (response: any) => {
          const indexToRemove = this.savedPokemons.findIndex(savedPokemon => savedPokemon.id == pokemon.id);

          const savedPokemonsCopy1 = [...this.savedPokemons];
          savedPokemonsCopy1.splice(indexToRemove, 1);
          this.savedPokemons = savedPokemonsCopy1;
        },
        (error: HttpErrorResponse) => {
          console.error(error.message);
        }
      )
    }
  }

  SwapDisplayedPokemons(event: any) {
    this.showSavedPokemons = event.checked;
  }

  public getPokemons(): void {
    this.pokemonService.getPokemons().subscribe(
      (response: any) => {
        this.isLoading = true;
        this.pokemons = response.results;
      },
      (error: HttpErrorResponse) => {
        console.error(error.message);
      }
    )
  }

  public getSavedPokemons(): void {
    this.pokemonService.getSavedPokemons().subscribe(
      (response: Array<any>) => {
        this.savedPokemons = response;
      },
      (error: HttpErrorResponse) => {
        console.error(error.message);
      }
    )
  }
}
